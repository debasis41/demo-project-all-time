function isPSVersionIsGreaterThan($passedVersion) {
    $installedVersion = Get-Host | Select-Object Version
    $statusCheck = $true
    if ([String]$installedVersion.Version -le $passedVersion) {
        $statusCheck = $false
    }

    $statusCheck
}
function Get-TimeStamp {
    return "[{0:MM/dd/yy} {0:HH:mm:ss}]" -f (Get-Date)
}

function outputStatement($printString) {
    $generatedString = "$(Get-TimeStamp) | " + $printString
    if(($installDetailsGLOBAL.LoggingMode -match "Shell") -or ($installDetailsGLOBAL.LoggingMode -match "Both")){
        Write-Output $generatedString
    }
    Write-Output $generatedString | Out-file $logFileNameGLOBAL -append
}

function removeOracleJavaFromPATH() {
    # Get it
    $path = [System.Environment]::GetEnvironmentVariable('PATH', 'Machine')
    
    if ($path -Like "*Java\javapath*") {
        $updatedPath = ""
        # Remove unwanted elements
        $path.Split(";") | ForEach-Object {
            if ($_ -NotLike "*Java\javapath*" ) {
                $updatedPath = $updatedPath + $_ + ';'
            }
        }
    
        $updatedPath = $updatedPath -replace ';;', ';'
        # Set it
        [System.Environment]::SetEnvironmentVariable('PATH', $updatedPath, 'Machine')
        outputStatement "Oracle Java\javapath references removed from PATH"
    }

}

function extractJavaZip($errorActionType) {
    $javaZipAddress = Join-Path $PSScriptRoot  ('amazon-corretto-' + $installDetailsGLOBAL.JreVersion + '-windows-x64-jre.zip')
    # Check if file exists locally
    $javaFileStatus = Test-Path -Path $javaZipAddress -PathType Leaf
    # Java File Not Found Locally, So Download
    try {
        if (-Not $javaFileStatus) {
            $Url = "https://corretto.aws/downloads/resources/" + $installDetailsGLOBAL.JreVersion + "/amazon-corretto-" + $installDetailsGLOBAL.JreVersion + "-windows-x64-jre.zip"
            Invoke-WebRequest -Uri $Url -OutFile $javaZipAddress -ErrorAction $errorActionType
        }
        Remove-Item (Join-Path $installDetailsGLOBAL.JavaInstallLocation "*") -Recurse -Force -ErrorAction SilentlyContinue
        Expand-Archive -LiteralPath  $javaZipAddress -DestinationPath $installDetailsGLOBAL.JavaInstallLocation 
    }
    catch {
        Write-Error "Java zip processing failed, stopping Script if setting up fresh Java: $($_)"
    }
}

function setJavaClassVariables() {
    $javaHomeAddress = Join-Path $installDetailsGLOBAL.JavaInstallLocation ("jre" + $installDetailsGLOBAL.JreVersion.split(".")[0])
    # Set JAVA_HOME
    [System.Environment]::SetEnvironmentVariable('JAVA_HOME', $javaHomeAddress, [System.EnvironmentVariableTarget]::Machine)
    $javaPathAddress = ";" + (Join-Path $javaHomeAddress "bin")
    # Adding Java to Path
    [System.Environment]::SetEnvironmentVariable('Path', [System.Environment]::GetEnvironmentVariable('Path', [System.EnvironmentVariableTarget]::Machine) + $javaPathAddress, [System.EnvironmentVariableTarget]::Machine)
    
    # CLASSPATH SET, so appending JAVA_HOME to it
    if ($null -ne [System.Environment]::GetEnvironmentVariable("CLASSPATH", "Machine")) {
        [System.Environment]::SetEnvironmentVariable('CLASSPATH', [System.Environment]::GetEnvironmentVariable('CLASSPATH', [System.EnvironmentVariableTarget]::Machine) + ";" + [System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine"), [System.EnvironmentVariableTarget]::Machine)
    }
    # CLASSPATH not Set, adding JAVA_HOME to it
    else {
        [System.Environment]::SetEnvironmentVariable('CLASSPATH', $javaHomeAddress, [System.EnvironmentVariableTarget]::Machine)
    }
}

function setupJavaIfRequired() {
    # Checking if Oracle Java is Installed, check suffices only for Windows
    
    # Setup java if
    # JAVA_HOME is not set i.e. Java not installed
    # Oracle Java installed AND JAVA_HOME is setup for Oracle AND we wish to move to Corretto
    # Java Setup Wont work if Coretto JDK is installed
    if ( ($null -eq [System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine")) -or
        ($null -ne [System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine") -and
            -Not([System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine") -Match "corretto") -and
            ($installDetailsGLOBAL.SwitchJavaToCorreto -eq 'TRUE'))) {
        outputStatement "JAVA_HOME not set yet"

        $oracleJavaInstalledStatus = Get-Package -Provider Programs -IncludeWindowsInstaller -Name *Java* -ErrorAction SilentlyContinue 

        if ($oracleJavaInstalledStatus) {
            # Remove Oracle Java
            uninstallSoftware "Java" $true
        }

        removeOracleJavaFromPATH
        extractJavaZip 'stop'
        setJavaClassVariables
       
        outputStatement "Java Setup Completed"
    }
    else {
        outputStatement "JAVA_HOME already Set, no need to install java"
    }
}

function stopService($serviceName, $hardStop){
    $service = Get-Service -Name $serviceName -ErrorAction SilentlyContinue
    # Service Exists
    if ($service.Length -gt 0) {
        if ($hardStop) {
            $ServicePID = (get-wmiobject win32_service | Where-Object { $_.name -eq $serviceName }).processID
            taskkill.exe /F /PID $ServicePID
        }
        else {
            Stop-Service $serviceName -Force
            $service.WaitForStatus("Stopped", '00:00:15')
        }
        outputStatement "'$serviceName' is Stopped"
    }
    else {
        outputStatement "'$serviceName' not found, please check service name again"
    }
}

function deleteService($serviceName, $isJCCDService){
    $service = Get-Service -Name $serviceName -ErrorAction SilentlyContinue
    # Service Exists
    if ($service.Length -gt 0) {
        if ($isJCCDService) {
           Set-Location $installDetailsGLOBAL.CentralizedControllerInstallPackage
           $jccdAddress = Join-Path $installDetailsGLOBAL.CentralizedControllerInstallPackage "jccd.exe"
           if( Test-Path -Path $jccdAddress -PathType Leaf){
            .\jccd.exe delete $serviceName
            Start-Sleep -s 5
            }
            Set-Location $PSScriptRoot
        }
        sc.exe delete $serviceName
        Start-Sleep -s 5
        outputStatement "'$serviceName' is Deleted"
    }
    else {
        outputStatement "'$serviceName' not found, please check service name again"
    }
}

function upgradeJavaIfPossible() {

    # Making Sure Process works only for Coretto
    if ($null -ne [System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine") -and 
        [System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine") -Match "corretto") {
        outputStatement "Checking for possible upgrade to Corretto Java"

        # The versioning system might change after upgrade to java 15 or java 16. Recheck code then
        $jvmDLLAddress = Join-Path ([System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine")) "bin" | Join-Path -Childpath "server" | Join-Path -ChildPath "jvm.dll"
        $jvmVersionCurrent = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($jvmDLLAddress).FileVersion
        $versionPartsCurrent = $jvmVersionCurrent -split '\.'
        $versionMinifiedCurrent = $versionPartsCurrent[0] + '.' + $versionPartsCurrent[2].Substring(0, 3)

        $versionPartsToBeInstalled = $installDetailsGLOBAL.JreVersion -split '\.'
        $versionMinifiedToBeInstalled = $versionPartsToBeInstalled[0] + '.' + $versionPartsToBeInstalled[1]

        if ([double]$versionMinifiedToBeInstalled -gt [double]$versionMinifiedCurrent) {
            outputStatement "Corretto Java Upgrade Possible"
            stopService "EmpirixCore" $false
            extractJavaZip 'silentlycontinue'
            outputStatement "Corretto Java Upgrade Done"
        }
        else {
            outputStatement "Corretto Java Upgrade Not Possible"
        }
    }
    else {
        outputStatement "Corretto Java not found, upgrade not possible"
    }
}

function uninstallSoftware($softwareName, $isWildcardEntry) {
    $isAppinstalled = Get-Package -Provider Programs -IncludeWindowsInstaller -Name ("*" + $softwareName + "*") -ErrorAction SilentlyContinue

    If ($isAppinstalled) {
        outputStatement "MSI(exe) variant of '$softwareName' is found on this machine."
        $applicationsInstalled = $null
        if ($isWildcardEntry) {
            $applicationsInstalled = Get-WmiObject -Class Win32_Product -Filter "Name like '%$($softwareName)%'"
        }
        else {
            $applicationsInstalled = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -eq $softwareName }
        }

        if ($applicationsInstalled) {
            $productGUIDs = $applicationsInstalled.IdentifyingNumber
            foreach ($item in $productGUIDs) {
                $arguments = "/norestart" + " " + "/q/x" + " " + $item
                Start-Process "msiexec.exe" -ArgumentList $arguments -Wait -NoNewWindow
            }
            outputStatement "MSI(exe) uninstall of '$softwareName' complete"
        }
        else {
            outputStatement "Problem Occured During MSI(exe) uninstall of '$softwareName'"
        }
    }
    else {
        outputStatement "MSI(exe) variant of '$softwareName' not found, uninstall not possible"
    }
}

function startIfServiceExists($serviceName) {
    $service = Get-Service -Name $serviceName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0) {
        try {
            $job_result = Start-Job -ScriptBlock { param($p1)
                Start-Service -Name $p1 -ErrorAction SilentlyContinue 
            } -ArgumentList $serviceName | Out-Null
			
            $job_id = $job_result.id
            while ($job_result.state -match 'running') {
                Start-Sleep -seconds 20
                $job_result = get-job $job_id
            }
        }
        catch {
            Write-Error "Encountered Error During Starting Service, Continuing Script: $($_)"
        }
        
    }
}

function importAndReturnConfigFile($fileName) {
    outputStatement "Importing and Validating Config File"
    $installDetailsGLOBAL = Get-Content $fileName | Out-String | ConvertFrom-Json
    $installDetailsGLOBAL = inputValidator $installDetailsGLOBAL

    $installDetailsGLOBAL
}

function packageCountChecker($searchString){
    # Ensure the correct of package*.zip files
    $packageZipFilesList = Get-ChildItem -Path $PSScriptRoot -recurse -filter $searchString | Group-Object -Property Directory
    if ($packageZipFilesList.count -ne 1){
        throw "Incorrect # of $searchString package in folder"
    }
}

function checkIfValidOrSetDefault($inputParameter, $defaultValue, $isAddress) {

    if ([string]::IsNullOrWhiteSpace($inputParameter)) {
        $inputParameter = $defaultValue
    }

    # Create Folder if absent, supress output
    if ($isAddress) {
        [void](New-Item -ItemType Directory -Force -Path $inputParameter)
    }

    $inputParameter
}

function inputValidator($jsonInput) {
    outputStatement "Checking inputs specified by user in config file i.e. InstallDetails.json"

    $jsonInput.LoggingMode = checkIfValidOrSetDefault $jsonInput.LoggingMode "Shell" $false
    $jsonInput.CentralizedControllerInstallPackage = checkIfValidOrSetDefault $jsonInput.CentralizedControllerInstallPackage "C:\Program Files\Empirix\Centralized Controller" $true
    $jsonInput.CentralizedControllerConfigLocation = checkIfValidOrSetDefault $jsonInput.CentralizedControllerConfigLocation "C:\Users\Administrator\Empirix\Centralized Controller" $true
    $jsonInput.JavaInstallLocation = checkIfValidOrSetDefault $jsonInput.JavaInstallLocation "C:\Program Files\Amazon Corretto" $true
    $jsonInput.PassiveBackupLocation = checkIfValidOrSetDefault $jsonInput.PassiveBackupLocation "C:\Backup" $true
    $jsonInput.SwitchJavaToCorreto = checkIfValidOrSetDefault $jsonInput.SwitchJavaToCorreto "TRUE" $false
    $jsonInput.JreVersion = checkIfValidOrSetDefault $jsonInput.JreVersion "8.292.10.1" $false
    $jsonInput.WipeMode = checkIfValidOrSetDefault $jsonInput.WipeMode "FALSE" $false
    $jsonInput.StartServicesAfterInstall = checkIfValidOrSetDefault $jsonInput.StartServicesAfterInstall "TRUE" $false
    $jsonInput.SoftwareToBeInstalled = checkIfValidOrSetDefault $jsonInput.SoftwareToBeInstalled "HammerManager" $false
    $jsonInput.WipeAndFreshReInstallMode = checkIfValidOrSetDefault $jsonInput.WipeAndFreshReInstallMode "FALSE" $false
    $jsonInput.JavaServiceMemoryStartSizeInMB = checkIfValidOrSetDefault $jsonInput.JavaServiceMemoryStartSizeInMB "1024" $false
    $jsonInput.JavaServiceMemoryMaxSizeInMB = checkIfValidOrSetDefault $jsonInput.JavaServiceMemoryMaxSizeInMB "3072" $false
    $jsonInput.ConfigFileDetails.HammerManagerConfig.RMQDetails = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.HammerManagerConfig.RMQDetails "msgadmin:123Empirix!@10.90.20.150" $false
    $jsonInput.ConfigFileDetails.HammerManagerConfig.ExternalRMQDetails = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.HammerManagerConfig.ExternalRMQDetails "" $false
    $jsonInput.ConfigFileDetails.HammerManagerConfig.RoleList = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.HammerManagerConfig.RoleList "HammerManagerRole,SchedulerRole,DatabaseRole" $false
    $jsonInput.ConfigFileDetails.HammerManagerConfig.DatabaseURL = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.HammerManagerConfig.DatabaseURL "jdbc:sqlserver://10.88.2.120;databaseName=vwdb;applicationIntent=ReadWrite;multiSubnetFailover=true" $false
    $jsonInput.ConfigFileDetails.HammerManagerConfig.DBUsername = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.HammerManagerConfig.sendcorequeue "sa" $false
    $jsonInput.ConfigFileDetails.HammerManagerConfig.DBPassword = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.HammerManagerConfig.recvqueuelist "Empirix!" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.RMQDetails = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.RMQDetails "msgadmin:123Empirix!@10.90.20.150" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.RoleList = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.RoleList "VoiceWatchRole,SchedulerRole" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.sendprobequeue = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.sendprobequeue "probe_us" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.sendcorequeue = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.sendcorequeue "core" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.recvqueuelist = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.recvqueuelist "jcc" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.StorageType = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.StorageType "network" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.StorageDomain = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.StorageDomain "wasabisys.com" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.StorageRegion = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.StorageRegion "us-east-1" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.StorageAccessKey = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.StorageAccessKey "AAAAAAAAAAAAAAAAAAAAAAA" $false
    $jsonInput.ConfigFileDetails.SchedulerConfig.StorageSecretKey = checkIfValidOrSetDefault $jsonInput.ConfigFileDetails.SchedulerConfig.StorageSecretKey "BBBBBBBBBBBBBBBBBBBBBBBBBBBBB" $false

    $jsonInput
}

function wipeInstallConfigFiles($deleteInstallFiles) {
    outputStatement "Wiping All Config Files"
    
    $searchPath = Join-Path $installDetailsGLOBAL.CentralizedControllerConfigLocation "*"
    Get-ChildItem -Path $searchPath -Include *.properties, *.json, *.xml | ForEach-Object { Remove-Item -Path $_.FullName }
    
    if ($deleteInstallFiles) {
        outputStatement "Wiping All Install Files"
        Remove-Item -Force -Recurse $installDetailsGLOBAL.CentralizedControllerInstallPackage
    }
}

function setupConfig($freshInstallStatus) {
    outputStatement "Setting up Config Files"

    $CentralizedControllerFilesLocation = Join-Path $PSScriptRoot "CentralizedControllerInstallPackage" | Join-Path -ChildPath "*"
    Copy-Item -Path $CentralizedControllerFilesLocation -Destination $installDetailsGLOBAL.CentralizedControllerInstallPackage -Recurse
    
    if ($freshInstallStatus) {
        if (($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Scheduler') -or
            ($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Both')) {
            Copy-Item -Path (Join-Path $PSScriptRoot "SchedulerConfigFiles" | Join-Path -ChildPath "*") -Destination $installDetailsGLOBAL.CentralizedControllerConfigLocation -Recurse -Force
        }


        if (($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'HammerManager') -or
            ($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Both')) {
            Copy-Item -Path (Join-Path $PSScriptRoot "HammerManagerConfigFiles" | Join-Path -ChildPath "*") -Destination $installDetailsGLOBAL.CentralizedControllerConfigLocation -Recurse -Force
        }
    }
    else {
        $CentralizedControllerConfigLocation = Join-Path $PSScriptRoot "CentralizedControllerConfigFiles" | Join-Path -ChildPath "*"
        Copy-Item -Path $CentralizedControllerConfigLocation -Destination $installDetailsGLOBAL.CentralizedControllerConfigLocation -Recurse -Force
    }

}

function backupConfigForActiveUse($localfolderName, $configFolderAddress) {
    $BackupDelete = Join-Path $PSScriptRoot $localfolderName | Join-Path -ChildPath "*"
    # Empty Folder
    Remove-Item -Force -Recurse $BackupDelete

    $Backup = Join-Path $PSScriptRoot $localfolderName
    $searchPath = Join-Path $configFolderAddress "*"

    # Backup Config
    Get-ChildItem -Path $searchPath -Include *.properties, *.json, *.xml | Copy-Item -Destination $Backup
}

function backupConfigForPassiveUse() {
    outputStatement "Making Passive Backup of CentralizedControllerConfig"
    $CCBackupLocation = Join-Path $passiveBackupFolderGLOBAL "CCConfigBackup"
    [void](New-Item -ItemType Directory -Path $CCBackupLocation -Force)
    Get-ChildItem -Path (Join-Path $installDetailsGLOBAL.CentralizedControllerConfigLocation "*") -Include *.config, *.properties, *.json, *.xml | Copy-Item -Destination $CCBackupLocation
}

function backupLogsAndScriptConfig(){
    outputStatement "Making Backup of Logs and Script Config"
    $LogsBackupLocation = Join-Path $passiveBackupFolderGLOBAL "LogsBackup"
    [void](New-Item -ItemType Directory -Path $LogsBackupLocation -Force)
    Get-ChildItem -Path (Join-Path $PSScriptRoot "*") -Include *.log | Copy-Item -Destination $LogsBackupLocation

    $ScriptsBackupLocation = Join-Path $passiveBackupFolderGLOBAL "ScriptConfigBackup"
    [void](New-Item -ItemType Directory -Path $ScriptsBackupLocation -Force)
    Get-ChildItem -Path (Join-Path $PSScriptRoot "*") -Include *.json | Copy-Item -Destination $ScriptsBackupLocation
}

function wipeInstallation() {
    outputStatement "Starting Wipe Module - Centralized Controller(HammerManager/Scheduler) will be removed"
    backupConfigForPassiveUse
    stopService "CentralizedController" $true
    deleteService "CentralizedController" $true
    stopService "EmpirixOneSightNTRemoteAgent" $false
    uninstallSoftware "Centralized Controller" $false
    wipeInstallConfigFiles  $true
    [Environment]::SetEnvironmentVariable("CC_USER_HOME", $null, "Machine")
}

function installCentralizedControllerService() {
    outputStatement "Setting up Centralized Controller Service"

    $jccdAddress = Join-Path $installDetailsGLOBAL.CentralizedControllerInstallPackage -ChildPath "jccd.exe"
    $classpath = Join-Path $installDetailsGLOBAL.CentralizedControllerInstallPackage -ChildPath "jCC.jar"
    $CentralizedControllerServiceDescription = "Empirix Centralized Controller " + $jCCJarVersionGLOBAL
    $javaDLL = Join-Path ([System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine")) -ChildPath "bin" | Join-Path -ChildPath "server" | Join-Path -ChildPath "jvm.dll"
    $Options = "-Dcc.user.home=" + [System.Environment]::GetEnvironmentVariable("CC_USER_HOME", "Machine")
	
    # Change Working Directory to Access jccd.exe or prunsrv
    Set-Location $installDetailsGLOBAL.CentralizedControllerInstallPackage

    # Setting up CentralizedController Service
    .\jccd.exe install CentralizedController `
        --DisplayName="Empirix Centralized Controller" `
        --Install=$jccdAddress --Classpath=$classpath --Jvm=$javaDLL --StartMode=jvm --StopMode=jvm `
        --JvmMs=$installDetailsGLOBAL.JavaServiceMemoryStartSizeInMB --JvmMx=$installDetailsGLOBAL.JavaServiceMemoryMaxSizeInMB --JvmOptions=$Options `
        --Description=$CentralizedControllerServiceDescription --Startup=auto --ServiceUser=LocalSystem --StartClass=com.empirix.cc.Service `
        --StopClass=com.empirix.cc.Service --StartMethod=start --StopMethod=stop

    # Change Working Directory to powershell folder
    Set-Location $PSScriptRoot
}

function populateConfigFiles() {
    outputStatement "Populating User-Specified Values Into Config Files"
	
	$CCConfigLocationWithSlashes = $installDetailsGLOBAL.CentralizedControllerConfigLocation -replace "\\", "\\"

    if (($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'HammerManager') -or
        ($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Both')) {
        $CentralizedControllerConfigLocation = Join-Path $PSScriptRoot "HammerManagerConfigFiles"

        $fileName = Join-Path $CentralizedControllerConfigLocation "centralizedcontroller.properties"
        (Get-Content $fileName) | Foreach-Object {
            $_ -replace 'ROLE_LIST', $installDetailsGLOBAL.ConfigFileDetails.HammerManagerConfig.RoleList `
                -replace 'CC_HOME_WITH_EXTRA_SLASHES', $CCConfigLocationWithSlashes `
                -replace 'RMQ_DETAILS', $installDetailsGLOBAL.ConfigFileDetails.HammerManagerConfig.RMQDetails 
        } | Set-Content $fileName

        $fileName = Join-Path $CentralizedControllerConfigLocation "centralizedcontroller.hammermanager.properties"
        (Get-Content $fileName) | Foreach-Object {
            $_ -replace 'HMR_EXTERNAL_RMQ_ADDRESS', $installDetailsGLOBAL.ConfigFileDetails.HammerManagerConfig.ExternalRMQDetails
        } | Set-Content $fileName
            
        $fileName = Join-Path $CentralizedControllerConfigLocation "centralizedcontroller.database.properties"
        (Get-Content $fileName) | Foreach-Object {
            $_ -replace 'DATABASE_URL', $installDetailsGLOBAL.ConfigFileDetails.HammerManagerConfig.DatabaseURL `
                -replace 'DB_USERNAME', $installDetailsGLOBAL.ConfigFileDetails.HammerManagerConfig.DBUsername `
                -replace 'DB_PASSWORD', $installDetailsGLOBAL.ConfigFileDetails.HammerManagerConfig.DBPassword
        } | Set-Content $fileName
    }

    if (($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Scheduler') -or
        ($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Both')) {
        $JCCConfigLocation = Join-Path $PSScriptRoot "SchedulerConfigFiles"

        $fileName = Join-Path $JCCConfigLocation "centralizedcontroller.properties"
        (Get-Content $fileName) | Foreach-Object {
            $_ -replace 'ROLE_LIST', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.RoleList `
				-replace 'CC_HOME_WITH_EXTRA_SLASHES', $CCConfigLocationWithSlashes `
                -replace 'RMQ_DETAILS', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.RMQDetails 
        } | Set-Content $fileName

        $fileName = Join-Path $JCCConfigLocation "centralizedcontroller.voicewatch.properties"
        (Get-Content $fileName) | Foreach-Object {
            $_ -replace 'SEND_PROBE_QUEUE_NAME', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.sendprobequeue `
                -replace 'SEND_CORE_QUEUE_NAME', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.sendcorequeue `
                -replace 'RECV_QUEUE_NAME', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.recvqueuelist `
                -replace 'DATA_STORAGE_TYPE', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.StorageType `
                -replace 'DATA_STORAGE_DOMAIN', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.StorageDomain `
                -replace 'DATA_STORAGE_REGION', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.StorageRegion `
                -replace 'DATA_ACCESS_KEY', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.StorageAccessKey `
                -replace 'DATA_ACCESS_SECRET_KEY', $installDetailsGLOBAL.ConfigFileDetails.SchedulerConfig.StorageSecretKey
        } | Set-Content $fileName
    }
}

function deleteLocalUnzippedConfig() {
    outputStatement "Deleting Local Copy of Config Files"
    Remove-Item -Force -Recurse (Join-Path $PSScriptRoot "CentralizedControllerConfigFiles") -ErrorAction Ignore
    Remove-Item -Force -Recurse (Join-Path $PSScriptRoot "SchedulerConfigFiles") -ErrorAction Ignore
    Remove-Item -Force -Recurse (Join-Path $PSScriptRoot "HammerManagerConfigFiles") -ErrorAction Ignore
    Remove-Item -Force -Recurse (Join-Path $PSScriptRoot "CentralizedControllerInstallPackage") -ErrorAction Ignore
}

function setupCCUSERHOME() {
    # CC_USER_HOME not set
    if ($null -eq [System.Environment]::GetEnvironmentVariable("CC_USER_HOME", "Machine")) {
        outputStatement "Setting up CC_USER_HOME variable"
        $value = $installDetailsGLOBAL.CentralizedControllerConfigLocation -replace "Centralized Controller" , '' `
            -replace "Empirix", ''
        $valueMinusSlash = $value -replace '\\\\', ''
        [System.Environment]::SetEnvironmentVariable('CC_USER_HOME', $valueMinusSlash, [System.EnvironmentVariableTarget]::Machine)
        outputStatement "Environment Variable CC_USER_HOME set as '$valueMinusSlash'"
    }
    # CC_USER_HOME already set
    else {
        $presentValueCCUSERHOME = [System.Environment]::GetEnvironmentVariable("CC_USER_HOME", "Machine")
        outputStatement "Environment Variable CC_USER_HOME already set as '$presentValueCCUSERHOME'"
    }
}

function startServicesIfAllowed() {
    if ($installDetailsGLOBAL.StartServicesAfterInstall -eq 'true') {
        outputStatement "Starting Services"
        Start-Sleep -Seconds 10
        startIfServiceExists 'CentralizedController'
        Start-Sleep -Seconds 5
        startIfServiceExists 'EmpirixCore'
        startIfServiceExists 'EmpirixOneSightNTRemoteAgent'
    }
    else {
        outputStatement "Services NOT started after Install"
    }
}

function displayMessageAccordingSoftwareToBeInstalled($message) {
    $generatedMessage = ""
    if ($installDetailsGLOBAL.SoftwareToBeInstalled -eq 'Both') {
        $generatedMessage = 'Hammer Manager And Scheduler' + $message
    }
    else {
        $generatedMessage = $installDetailsGLOBAL.SoftwareToBeInstalled + ' ' + $message
    }
    outputStatement $generatedMessage
}

function setupFreshCentralizedController($freshInstallStatus) {
    outputStatement "Setting up Fresh Version of Centralized Controller"
    displayMessageAccordingSoftwareToBeInstalled 'will be installed'

    setupJavaIfRequired
    # Coretto Java is Installed and user has enabled switch to Coretto
    if (([System.Environment]::GetEnvironmentVariable("JAVA_HOME", "Machine") -Match "corretto") -and
        ($installDetailsGLOBAL.SwitchJavaToCorreto -eq 'TRUE')) {
        upgradeJavaIfPossible
    }

    if ($freshInstallStatus) {
        populateConfigFiles 
    }
    setupConfig $freshInstallStatus

    setupCCUSERHOME
    installCentralizedControllerService
    startServicesIfAllowed
    # setupCCServiceDescription

    deleteLocalUnzippedConfig
    displayMessageAccordingSoftwareToBeInstalled 'install is complete'
    outputStatement "CentralizedController Install Complete" 
}

function getVersionFromCCService() {
    $description = Get-WmiObject win32_service | Where-Object { $_.Name -like 'CentralizedController' } | Select-Object Description
    $version = $description -replace "[^0-9.]" , ''

    $version
}

function startProcess() {
    $machineName = [System.Net.Dns]::GetHostName()
    outputStatement "Starting CentralizedController Install Script Now on $machineName"
    deleteLocalUnzippedConfig
    if ($installDetailsGLOBAL.WipeAndFreshReInstallMode -eq 'TRUE') {
        outputStatement "Starting Wipe and Fresh Reinstall Mode"
        wipeInstallation
        deleteLocalUnzippedConfig
        setupFreshCentralizedController $true
    }
    elseif ($installDetailsGLOBAL.WipeMode -eq 'TRUE') {
        outputStatement "Starting Wipe Mode"
        wipeInstallation
        deleteLocalUnzippedConfig 
    }
    else {
        outputStatement "Starting Install/Upgrade Mode - Centralized Controller(HammerManager/Scheduler) will be installed"
        $installedCentralizedControllerjarLocation = Join-Path $installDetailsGLOBAL.CentralizedControllerInstallPackage "jCC.jar"
        $installedCentralizedControllerjarFoundStatus = Test-Path -Path $installedCentralizedControllerjarLocation -PathType Leaf
    
        outputStatement "Unzipping Files from package.zip"
        $zipAddress = Join-Path $PSScriptRoot "jcc_package*.zip"
        try {
            Expand-Archive -Path $zipAddress -DestinationPath $PSScriptRoot -ErrorAction Stop
        }
        catch {
            Write-Error "Unzip Failed, Stopping Script: $($_)"
        }
    
        if (
            $installedCentralizedControllerjarFoundStatus -and  
            (Get-Service "CentralizedController" -ErrorAction SilentlyContinue)) {
            outputStatement "A version of CentralizedController Service(HammerManager/Scheduler) is Installed"
                
            $installedCentralizedControllerVersion = getVersionFromCCService

            outputStatement "Installed JCC is: $installedCentralizedControllerVersion"
            outputStatement "Latest JCC is: $jCCJarVersionGLOBAL"
            
            if ([System.Version]$jCCJarVersionGLOBAL -gt [System.Version]$installedCentralizedControllerVersion) {
                outputStatement "Upgrade is Possible"
    
                New-Item -Path . -Name "CentralizedControllerConfigFiles" -ItemType "directory" -ErrorAction SilentlyContinue | Out-Null
    
                backupConfigForActiveUse "CentralizedControllerConfigFiles" $installDetailsGLOBAL.CentralizedControllerConfigLocation
                    
                wipeInstallation
    
                setupFreshCentralizedController $false
            }
            else {
                outputStatement "Upgrade not Possible, check versions"
                deleteLocalUnzippedConfig
            }
        }
        else {
            outputStatement "CentralizedController Software Not Found, Starting Fresh Install"
            setupFreshCentralizedController $true
        }
    }
    
    outputStatement "CentralizedController Install Script Has Ended, Bye"
    backupLogsAndScriptConfig
}

if (isPSVersionIsGreaterThan "5.0") {
    try {
        Get-ChildItem -Path $PSScriptRoot *.log | ForEach-Object { Remove-Item -Path $_.FullName }
        # Exception Handling Will Stop Program if Exception Found
        $ErrorActionPreference = "Stop"
        $transcripts = Join-Path $PSScriptRoot "Transcripts.log"
		# Starting Transcripts to track warning and errors		
        Start-Transcript -path $transcripts -append

        Set-Location $PSScriptRoot	
        $logFileNameGLOBAL = Join-Path $PSScriptRoot "HmrMgrSchdInstallScript.log"	
        $jsonFileAddress = Join-Path $PSScriptRoot 'InstallDetails.json'

        # Ensure the correct of package*.zip files
        packageCountChecker "jcc_package*.zip"

        $packageZipFilesList = Get-ChildItem -Path $PSScriptRoot -recurse -filter jcc_package*.zip | Group-Object -Property Directory
        $jCCJarVersionGLOBAL = $packageZipFilesList.Group.Name -replace "[^0-9.]" , ''
        $jCCJarVersionGLOBAL = $jCCJarVersionGLOBAL.Substring(0, $jCCJarVersionGLOBAL.Length - 1)

        # Import Values from JSON File, and set defaults if required
        $installDetailsGLOBAL = importAndReturnConfigFile $jsonFileAddress

        # Creating Backup Folder Structure
        $passiveBackupSessionFolder = (Get-Date -Format "yyyy_MM_dd_HH_mm").ToString() + "_CC_Config"
        $passiveBackupFolderGLOBAL = Join-Path $installDetailsGLOBAL.PassiveBackupLocation $passiveBackupSessionFolder
        New-Item -Path $installDetailsGLOBAL.PassiveBackupLocation -Name $passiveBackupSessionFolder -ItemType "directory" -ErrorAction SilentlyContinue | Out-Null
        
        startProcess
    }
    catch {
        Write-Error "An Error Occured: $($_)"
    }
    finally {
        Stop-Transcript
    }
}
else {
    outputStatement "Script Cannot run as PowerShell Version is lesser than 5.0"
}
